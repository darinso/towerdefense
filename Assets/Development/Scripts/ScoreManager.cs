﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Utility;
using System;

namespace TowerDefense
{
    public class ScoreManager : SingletonBehaviour<ScoreManager>
    {
        protected float score = 0f;

        public event Action<float> ScoreChanged;
        protected void OnScoreChanged(float newScore)
        {
            if (ScoreChanged != null)
                ScoreChanged(newScore);
        }

        public virtual void AddScore(float points)
        {
            score += points;
            OnScoreChanged(score);
        }

        public virtual float GetScore()
        {
            return score;
        }
    }
}
