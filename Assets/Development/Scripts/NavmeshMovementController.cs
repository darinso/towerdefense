﻿using Framework.Core.ObjectPool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;

namespace TowerDefense
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class NavmeshMovementController : MovementController, IPoolable
    {
        private NavMeshAgent navMeshAgent;
        private bool needRestart = true;

        public void ResetState()
        {
            needRestart = true;
        }

        protected virtual void Awake()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
        }

        protected override void Update()
        {
            base.Update();

            if (needRestart)
            {
                needRestart = false;
                navMeshAgent.destination = targetPosition.position;
            }
        }
    }
}
