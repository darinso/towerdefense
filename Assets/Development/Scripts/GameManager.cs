﻿using Framework.Utility;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TowerDefense
{
    public class GameManager : PersistentSingleton<GameManager>
    {
        protected GameSettings gameSettingsInternal;
        protected GameSettings GameSettings
        {
            get
            {
                if (gameSettingsInternal == null)
                    gameSettingsInternal = Resources.Load<GameSettings>("GameSettings");

                return gameSettingsInternal;
            }
        }

        protected DifficultySettings settings;

        public void SetDifficulty(int difficulty)
        {
            GameSettings.difficulty = difficulty;
            UpdateSettings();
        }

        public void LoadMainMenu()
        {
            SceneManager.LoadScene(0);
        }

        public void LoadMainLevel()
        {
            SceneManager.LoadScene(1);
        }

        public DifficultySettings GetSettings()
        {
            if (settings == null)
                UpdateSettings();

            return settings;
        }

        private void UpdateSettings()
        {
            if (GameSettings.difficulties == null || GameSettings.difficulties.Count < GameSettings.difficulty)
            {
                settings = new DifficultySettings();
                Debug.LogError("Cannot find difficulty");
            }
            else
            {
                settings = GameSettings.difficulties[GameSettings.difficulty];
            }
        }
    }
}
