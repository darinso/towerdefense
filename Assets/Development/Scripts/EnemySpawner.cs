﻿using Framework.Core.ObjectPool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class EnemySpawner : Spawner
    {
        protected override void SpawnOneObject(GameObject prefab, Vector3 position, Quaternion rotation)
        {
            var enemyObj = GameObjectPool.Instantiate(prefab, position, rotation);
            EnemyManager.Instance.AddEnemy(enemyObj);
        }
    }
}