﻿using Framework.Core.ObjectPool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField]
        protected GameObject prefabToSpawn;

        [SerializeField]
        protected SpawnArea spawnArea;

        [SerializeField]
        protected float spawnIntervalMin = 1f;

        [SerializeField]
        protected float spawnIntervalMax = 1f;

        protected float timeToSpawnLeft = 0f;

        public void SetSpawnInterval(float spawnIntervalMin, float spawnIntervalMax)
        {
            this.spawnIntervalMin = spawnIntervalMin;
            this.spawnIntervalMax = spawnIntervalMax;
        }

        protected void Start()
        {
            timeToSpawnLeft = Random.Range(spawnIntervalMin, spawnIntervalMax);
        }

        protected void Update()
        {
            timeToSpawnLeft -= Time.deltaTime;
            if (timeToSpawnLeft <= 0)
            {
                Spawn();
                timeToSpawnLeft = Random.Range(spawnIntervalMin, spawnIntervalMax);
            }
        }

        protected virtual void Spawn()
        {
            spawnArea.NextPoint();
            SpawnOneObject(prefabToSpawn, spawnArea.GetPosition(), spawnArea.GetRotation());
        }

        protected virtual void SpawnOneObject(GameObject prefab, Vector3 position, Quaternion rotation)
        {
            GameObjectPool.Instantiate(prefab, position, rotation);
        }
    }
}