﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class MovementController : MonoBehaviour
    {
        [SerializeField]
        protected Transform targetPosition;
        [SerializeField]
        protected float minDistance = 1f;

        public event Action<MovementController> DestinationReached;
        protected void OnDestinationReached()
        {
            if (DestinationReached != null)
                DestinationReached(this);
        }

        public virtual Transform GetTargetPosition()
        {
            return targetPosition;
        }

        public virtual void SetTargetPosition(Transform targetPosition)
        {
            this.targetPosition = targetPosition;
        }
        
        protected virtual void Update()
        {
            if (Vector3.Distance(transform.position, targetPosition.position) <= minDistance)
                OnDestinationReached();
        }
    }
}