﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class SmartDamageReceiver : DamageReceiver
    {
        [SerializeField]
        private bool friendlyFireEnabled = false;

        public override void ApplyDamage(object damageOwner, float amount)
        {
            if (damageOwner != null & damageOwner != owner)
                base.ApplyDamage(damageOwner, amount);
            else if (friendlyFireEnabled)
                base.ApplyDamage(damageOwner, amount);
        }
    }
}
