﻿using Framework.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense
{
    public class GameUIController : SingletonBehaviour<GameUIController>
    {
        [SerializeField]
        private Button mainMenuButon;

        [SerializeField]
        private Text scoreCount;

        [SerializeField]
        private Text lifeCount;

        [SerializeField]
        private Button finalMainMenuButton;

        [SerializeField]
        private Text finalScoreCount;

        [SerializeField]
        private GameObject[] finalShowObjects;

        [SerializeField]
        private GameObject[] mainGameObjects;

        void Start()
        {
            mainMenuButon.onClick.AddListener(ReturnToMainMenu);
            finalMainMenuButton.onClick.AddListener(ReturnToMainMenu);

            ScoreManager.Instance.ScoreChanged += (newScore) => scoreCount.text = newScore.ToString();

            scoreCount.text = 0f.ToString();

            var point = FindObjectOfType<TargetPoint>();
            var dr = point.GetComponent<DamageReceiver>();
            lifeCount.text = dr.CurrentLife.ToString();

            dr.Damaged += (a, owner, amount) => lifeCount.text = a.CurrentLife.ToString();
            
            ShowMainScreen();
        }

        private void ReturnToMainMenu()
        {
            SessionController.Instance.ReturnToMainMenu();
        }

        private void ShowMainScreen()
        {
            for (int i = 0; i < mainGameObjects.Length; i++)
                mainGameObjects[i].SetActive(true);
            for (int i = 0; i < finalShowObjects.Length; i++)
                finalShowObjects[i].SetActive(false);
        }

        public void ShowGameOverScreen()
        {
            for (int i = 0; i < mainGameObjects.Length; i++)
                mainGameObjects[i].SetActive(false);
            for (int i = 0; i < finalShowObjects.Length; i++)
                finalShowObjects[i].SetActive(true);

            finalScoreCount.text = ScoreManager.Instance.GetScore().ToString();
        }
    }
}
