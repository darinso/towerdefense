﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class RectangleSpawnArea : SpawnArea
    {
        public Vector3 size;
        public Vector3 notSpawnSize;

        protected Vector3 currentPoint;

        protected int MAX_ITERATIONS = 1000;

        public override void NextPoint()
        {
            var iter = 0;
            while (iter < MAX_ITERATIONS)
            {
                iter++;
                var x = Random.Range(transform.position.x - size.x / 2, transform.position.x + size.x / 2);
                var y = Random.Range(transform.position.y - size.y / 2, transform.position.y + size.y / 2);
                var z = Random.Range(transform.position.z - size.z / 2, transform.position.z + size.z / 2);
                currentPoint = new Vector3(x, y, z);

                if (!IsPointInsideBox(currentPoint, transform.position, notSpawnSize))
                    break;
            }

            if (iter >= MAX_ITERATIONS)
                Debug.LogError("Cannot produce point");
        }

        public override Vector3 GetPosition()
        {
            return currentPoint;
        }

        public override Quaternion GetRotation()
        {
            return transform.rotation;
        }

        protected void OnDrawGizmos()
        {
            Gizmos.color = new Color(1f, 0f, 1f, 0.3f);
            Gizmos.DrawCube(transform.position, size);
            Gizmos.color = new Color(0f, 1f, 0f, 0.3f);
            Gizmos.DrawCube(transform.position, notSpawnSize);
        }

        protected bool IsPointInsideBox(Vector3 point, Vector3 center, Vector3 size)
        {
            return IsFloatInsideRange(point.x, center.x - size.x / 2, center.x + size.x / 2) &
                IsFloatInsideRange(point.y, center.y - size.y / 2, center.y + size.y / 2) &
                IsFloatInsideRange(point.z, center.z - size.z / 2, center.z + size.z / 2);
        }

        protected bool IsFloatInsideRange(float value, float min, float max)
        {
            return value >= min & value <= max;
        }
    }
}