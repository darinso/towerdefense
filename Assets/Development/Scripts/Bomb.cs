﻿using Framework.Core.ObjectPool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace TowerDefense
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Collider))]
    public class Bomb : MonoBehaviour, IPoolable
    {
        protected object damageOwner;

        [SerializeField]
        protected LayerMask mask;

        [SerializeField]
        protected float damage = 10f;

        [SerializeField]
        protected float damageRadius;

        public void SetOwner(object owner)
        {
            damageOwner = owner;
        }

        protected void OnCollisionEnter(Collision collision)
        {
            var colliders = Physics.OverlapSphere(transform.position, damageRadius, mask);
            for (int i = 0; i < colliders.Length; i++)
            {
                var dr = colliders[i].GetComponent<DamageReceiver>();
                if (dr != null)
                    dr.ApplyDamage(damageOwner, damage);
            }
            GameObjectPool.Destroy(gameObject);
        }

        public void ResetState()
        {
            // needed for expanding
        }
    }
}
