﻿using Framework.Core.ObjectPool;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    [RequireComponent(typeof(MovementController))]
    [RequireComponent(typeof(DamageReceiver))]
    public class Enemy : MonoBehaviour, IPoolable
    {
        protected object damageOwner;

        [SerializeField]
        protected float points = 1;

        [SerializeField]
        protected float damage = 1;

        public virtual void Init(object damageOwner)
        {
            this.damageOwner = damageOwner;
            var dr = GetComponent<DamageReceiver>();
            dr.SetOwner(damageOwner);
        }

        private void Start()
        {
            var mc = GetComponent<MovementController>();
            mc.DestinationReached += (a) =>
            {
                var targetDR = a.GetTargetPosition().GetComponentInParent<DamageReceiver>();
                if (targetDR != null)
                    targetDR.ApplyDamage(damageOwner, damage);

                Destroy(gameObject);
            };

            var dr = GetComponent<DamageReceiver>();
            dr.Destroyed += (a, owner) =>
            {
                ScoreManager.Instance.AddScore(points);
                GameObjectPool.Destroy(gameObject);
            };
        }

        public void ResetState()
        {

        }
    }
}
