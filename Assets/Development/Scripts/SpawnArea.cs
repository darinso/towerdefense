﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class SpawnArea : MonoBehaviour
    {
        public virtual void NextPoint()
        {

        }

        public virtual Vector3 GetPosition()
        {
            return transform.position;
        }

        public virtual Quaternion GetRotation()
        {
            return transform.rotation;
        }
    }
}