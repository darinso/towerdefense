﻿using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense
{
    public class MainMenuUIController : MonoBehaviour
    {
        [SerializeField]
        private Button easyButton;

        [SerializeField]
        private Button normalButton;

        [SerializeField]
        private Button hardButton;

        [SerializeField]
        private Button exitButton;

        private void Start()
        {
            easyButton.onClick.AddListener(() => StartDifficulty(0));
            normalButton.onClick.AddListener(() => StartDifficulty(1));
            hardButton.onClick.AddListener(() => StartDifficulty(2));

            exitButton.onClick.AddListener(Exit);
        }

        private void StartDifficulty(int difficulty)
        {
            GameManager.Instance.SetDifficulty(difficulty);
            GameManager.Instance.LoadMainLevel();
        }

        private void Exit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}