﻿using Framework.Core.ObjectPool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField]
        protected LayerMask mask;

        [SerializeField]
        protected float height;

        [SerializeField]
        protected GameObject prefab;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, float.MaxValue, mask))
                {
                    var position = hit.point + Vector3.up * height;
                    var bombObj = GameObjectPool.Instantiate(prefab, position, Quaternion.identity);
                    var bomb = bombObj.GetComponent<Bomb>();
                    bomb.SetOwner(this);
                }
            }
        }
    }
}