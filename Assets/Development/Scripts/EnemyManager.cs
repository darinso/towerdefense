﻿using Framework.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class EnemyManager : SingletonBehaviour<EnemyManager>
    {
        private List<Enemy> aliveEnemies = new List<Enemy>();

        [SerializeField]
        private Transform targetPosition;

        protected override void Awake()
        {
            base.Awake();
        }

        public void SetTargetPosition(Transform targetPosition)
        {
            this.targetPosition = targetPosition;
        }

        public void AddEnemy(GameObject enemyObj)
        {
            var enemy = enemyObj.GetComponent<Enemy>();
            enemy.Init(gameObject);
            var dr = enemyObj.GetComponent<DamageReceiver>();
            dr.Destroyed += (a, owner) => aliveEnemies.Remove(a.GetComponent<Enemy>());
            var mc = enemyObj.GetComponent<MovementController>();
            mc.SetTargetPosition(targetPosition);

            aliveEnemies.Add(enemy);
        }
    }
}
