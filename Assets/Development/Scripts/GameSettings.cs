﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace TowerDefense
{
    public class GameSettings : ScriptableObject
    {
        public int difficulty = 0;
        public List<DifficultySettings> difficulties;
        
#if UNITY_EDITOR
        [MenuItem("Assets/Create/GameSettings")]
        public static void CreateGameSettings()
        {
            GameSettings asset = ScriptableObject.CreateInstance<GameSettings>();

            AssetDatabase.CreateAsset(asset, "Assets/Resources/GameSettings.asset");
            AssetDatabase.SaveAssets();

            EditorUtility.FocusProjectWindow();

            Selection.activeObject = asset;
        }
#endif
    }

    [System.Serializable]
    public class DifficultySettings
    {
        public float minSpawnTime = 1f;
        public float maxSpawnTime = 2f;
        public float lifeCount = 50f;
    }
}