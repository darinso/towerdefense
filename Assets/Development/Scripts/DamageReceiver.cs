﻿using Framework.Core.ObjectPool;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TowerDefense
{
    public class DamageReceiver : MonoBehaviour, IPoolable
    {
        [SerializeField]
        protected float currentLife;
        public float CurrentLife
        {
            get { return currentLife; }
        }

        [SerializeField]
        protected float maxLife;
        public float MaxLife
        {
            get { return maxLife; }
        }

        protected object owner;

        public void SetOwner(object newOwner)
        {
            owner = newOwner;
        }

        public event Action<DamageReceiver, object> Destroyed;
        protected void OnDestroyed(object damageOwner)
        {
            if (Destroyed != null)
                Destroyed(this, damageOwner);
        }

        public event Action<DamageReceiver, object, float> Damaged;
        protected void OnDamaged(object damageOwner, float amount)
        {
            if (Damaged != null)
                Damaged(this, damageOwner, amount);
        }

        protected virtual void Start()
        {
            ResetState();
        }

        public virtual void SetLife(float newMaxLife, float newCurrentLife)
        {
            maxLife = newMaxLife;
            currentLife = newCurrentLife;
        }

        public virtual void ApplyDamage(object damageOwner, float amount)
        {
            if (currentLife <= 0f)
                return;

            currentLife -= amount;

            if (currentLife <= 0f)
                OnDestroyed(damageOwner);
            else
                OnDamaged(damageOwner, amount);
        }

        public void ResetState()
        {
            currentLife = maxLife;
        }

        //private void OnDestroy()
        //{
        //    OnDestroyed(null);
        //}
    }
}
