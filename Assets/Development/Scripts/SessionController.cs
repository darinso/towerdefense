﻿using Framework.Utility;
using UnityEngine;

namespace TowerDefense
{
    public class SessionController : SingletonBehaviour<SessionController>
    {
        protected override void Awake()
        {
            base.Awake();

            Random.InitState(System.Environment.TickCount);

            var settings = GameManager.Instance.GetSettings();
            var spawners = FindObjectsOfType<EnemySpawner>();
            for (int i = 0; i < spawners.Length; i++)
                spawners[i].SetSpawnInterval(settings.minSpawnTime, settings.maxSpawnTime);

            var point = FindObjectOfType<TargetPoint>();
            var dr = point.GetComponent<DamageReceiver>();
            dr.SetLife(settings.lifeCount, settings.lifeCount);
            dr.Destroyed += (a, b) => GameOver();
            dr.SetOwner(FindObjectOfType<PlayerController>());

            EnemyManager.Instance.SetTargetPosition(point.transform);
        }

        public void ReturnToMainMenu()
        {
            Time.timeScale = 1f;
            GameManager.Instance.LoadMainMenu();
        }

        public void GameOver()
        {
            Time.timeScale = 0f;

            GameUIController.Instance.ShowGameOverScreen();
            //GameManager.Instance.LoadMainMenu();
        }
    }
}