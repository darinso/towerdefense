﻿using UnityEngine;

namespace Framework.Utility
{
    public class PersistentSingleton<T> : SingletonBehaviour<T> where T : MonoBehaviour
    {
        protected override void Awake()
        {
            if (Application.isPlaying)
                DontDestroyOnLoad(gameObject);
            
            base.Awake();
        }
    }
}