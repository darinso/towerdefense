﻿using UnityEngine;

namespace Framework.Utility
{
    public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance;
        private static bool isQuitting;
        private static readonly object syncRoot = new object();

        public static T Instance
        {
            get
            {
                lock (syncRoot)
                {
                    return GetInstance();
                }
            }

            private set
            {
                lock (syncRoot)
                {
                    instance = value;
                }
            }
        }

        private static T GetInstance()
        {
            if (instance != null)
                return instance;

            if (isQuitting)
                return null;

            var objects = FindObjectsOfType<T>();
            if (objects.Length > 0)
            {
                instance = objects[0];
                for (var i = 1; i < objects.Length - 1; i++)
                    DestroyImmediate(objects[i]);

                return instance;
            }

            var type = typeof(T);
            var goName = string.Format("_{0}", type.Name);
            var go = new GameObject(goName);
            instance = go.AddComponent<T>();
            Debug.Log(string.Format("Instantiating new singleton {0}", goName));

            return instance;
        }

        protected virtual void Awake()
        {
            lock (syncRoot)
            {
                if (instance == null)
                {
                    instance = this as T;
                }
                else if (instance != this)
                {
                    Destroy(gameObject);
                }
            }
        }

        protected virtual void OnDestroy()
        {
            lock (syncRoot)
            {
                if (instance == this)
                    instance = null;
            }
        }

        private void OnApplicationQuit()
        {
            isQuitting = true;
        }
    }
}