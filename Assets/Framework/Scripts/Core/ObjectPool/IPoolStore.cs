﻿using System.Collections.Generic;

namespace Framework.Core.ObjectPool
{
    public interface IPoolStore<T>
    {
        IEnumerable<T> FreeItems { get; }
        IEnumerable<T> UsedItems { get; }

        int FreeItemsCount { get; }
        int UsedItemsCount { get; }

        T Fetch();
        void Collect(T item);
    }
}