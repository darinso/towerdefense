﻿using System.Collections.Generic;

namespace Framework.Core.ObjectPool
{
    public class PoolStore<T> : IPoolStore<T>
    {
        #region Fields

        private readonly LinkedList<T> freeItems;
        private readonly LinkedList<T> usedItems;

        #endregion

        public PoolStore()
        {
            freeItems = new LinkedList<T>();
            usedItems = new LinkedList<T>();
        }

        public static IPoolStore<T> Create()
        {
            return new PoolStore<T>();
        }

        #region Implementation

        public IEnumerable<T> FreeItems
        {
            get { return freeItems; }
        }

        public IEnumerable<T> UsedItems
        {
            get { return usedItems; }
        }

        public int FreeItemsCount
        {
            get { return freeItems.Count; }
        }

        public int UsedItemsCount
        {
            get { return usedItems.Count; }
        }

        public T Fetch()
        {
            var item = freeItems.First.Value;
            if (item != null)
            {
                usedItems.AddLast(item);
                freeItems.RemoveFirst();
            }

            return item;
        }

        public void Collect(T item)
        {
            if (item == null || freeItems.Contains(item))
                return;

            usedItems.Remove(item);
            freeItems.AddLast(item);
        }

        #endregion
    }
}