﻿using System.Collections.Generic;

namespace Framework.Core.ObjectPool
{
    public class ObjectPool<T>
    {
        #region Fields

        private readonly Dictionary<string, IPoolStore<T>> objects;

        #endregion

        public ObjectPool()
        {
            objects = new Dictionary<string, IPoolStore<T>>();
        }

        #region API

        public void Reserve(string key, T item, int count)
        {
            var store = GetStore(key);
            for (var i = 0; i < count; i++)
                store.Collect(item);
        }

        public T Get(string key)
        {
            var store = GetStore(key);
            if (store.FreeItemsCount > 0)
                return store.Fetch();

            return default(T);
        }

        public void Release(string key, T item)
        {
            if (item == null)
                return;

            var store = GetStore(key);
            store.Collect(item);
        }

        #endregion

        private IPoolStore<T> GetStore(string key)
        {
            IPoolStore<T> poolStore;
            if (!objects.TryGetValue(key, out poolStore))
            {
                poolStore = PoolStore<T>.Create();
                objects.Add(key, poolStore);
            }

            return poolStore;
        }
    }
}