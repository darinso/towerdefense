﻿namespace Framework.Core.ObjectPool
{
    public interface IPoolable
    {
        void ResetState();
    }
}