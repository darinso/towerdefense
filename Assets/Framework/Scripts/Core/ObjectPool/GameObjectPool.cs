﻿using System.Linq;
using UnityEngine;

namespace Framework.Core.ObjectPool
{
    public static class GameObjectPool
    {
        #region Static Fields and Constants

        private static readonly ObjectPool<GameObject> pool = new ObjectPool<GameObject>();

        #endregion

        #region API

        public static GameObject Instantiate(string key, GameObject prefab, Vector3 pos, Quaternion rot, bool reserve = false)
        {
            if (string.IsNullOrEmpty(key) || prefab == null)
                return null;

            GameObject go = null;

            if (!reserve)
                go = pool.Get(key);

            if (go == null)
            {
                go = Object.Instantiate(prefab, pos, rot) as GameObject;
                go.name = key;
            }
            else
            {
                go.transform.position = pos;
                go.transform.rotation = rot;
                go.SetActive(true);
            }

            return go;
        }

        public static GameObject Instantiate(string key, GameObject prefab, bool reserve = false)
        {
            return Instantiate(key, prefab, Vector3.zero, Quaternion.identity, reserve);
        }

        public static GameObject Instantiate(GameObject prefab)
        {
            return prefab == null ? null : Instantiate(prefab.name, prefab);
        }

        public static GameObject Instantiate(GameObject prefab, Vector3 pos, Quaternion rot)
        {
            return prefab == null ? null : Instantiate(prefab.name, prefab, pos, rot);
        }

        public static bool Destroy(string key, GameObject go)
        {
            if (string.IsNullOrEmpty(key) || go == null)
                return false;

            Reset(go);

            go.SetActive(false);
            pool.Release(key, go);

            return true;
        }

        public static bool Destroy(GameObject go)
        {
            return go != null && Destroy(go.name, go);
        }

        public static GameObject Reserve(string key, GameObject prefab)
        {
            var go = Instantiate(key, prefab, true);
            Destroy(key, go);

            return go;
        }

        #endregion

        private static void Reset(GameObject obj)
        {
            if (obj == null)
                return;

            var poolableItems = obj.GetComponentsInChildren(typeof (IPoolable));
            foreach (var pItem in poolableItems.OfType<IPoolable>())
            {
                pItem.ResetState();
            }
        }
    }
}